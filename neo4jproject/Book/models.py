from django.db import models
from neomodel import StructuredNode,StringProperty, UniqueIdProperty, DateProperty, EmailProperty, RelationshipTo
from django_neomodel import DjangoNode 

class Book(DjangoNode):
    ID = UniqueIdProperty()
    code = StringProperty(unique_index=True, required=True, primary_key=True)
    name = StringProperty(index=True)
    class Meta:
        app_label = 'Book'

class User(DjangoNode):
    ID = UniqueIdProperty()
    fullname =StringProperty()
    username = StringProperty()
    password = StringProperty()
    email = EmailProperty(unique_index = True)

    # Relations :
    book = RelationshipTo('Book', 'pushlish')
    class Meta:
        app_label = 'Book'